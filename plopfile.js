module.exports = plop => {
  plop.setGenerator('model', {
    description: 'Create an example Mobx State Tree implementation',
    prompts: [
      {
        type: 'checkbox',
        name: 'includeApp',
        message: 'Would you like to override your app.tsx file with a Provider implementation for the root tree?',
        choices: [
          { name: "Yes - bear in mind you will lose any code changes you have made to app.tsx (recommended)", value: "yes" },
          { name: "No - you'll need to manually ensure you implement a provider for your app component", value: "no"}
        ]
      },
    ],
    actions: (data) => {
      let actions = [
        {
          type: 'add',
          force: true,
          path: 'babel.config.js',
          templateFile: 'plop-templates/mst/babel.config.js',
        },
        {
          type: 'add',
          path: 'app/models/index.ts',
          templateFile: 'plop-templates/mst/index.ts',
        },
        {
          type: 'add',
          path: 'app/models/application-model.ts',
          templateFile: 'plop-templates/mst/application-model.ts',
        },
        {
          type: 'add',
          path: 'app/models/root-model.ts',
          templateFile: 'plop-templates/mst/root-model.ts',
        },
        {
          type: 'add',
          path: 'app/models/job-model.ts',
          templateFile: 'plop-templates/mst/job-model.ts',
        },
        {
          type: 'add',
          path: 'app/models/jobs-model.ts',
          templateFile: 'plop-templates/mst/jobs-model.ts',
        },
        {
          type: 'add',
          path: 'app/models/user-model.ts',
          templateFile: 'plop-templates/mst/user-model.ts',
        },
        {
          type: 'add',
          path: 'app/models/setup.ts',
          templateFile: 'plop-templates/mst/setup.ts',
        },
        {
          type: 'add',
          path: 'app/models/environment.ts',
          templateFile: 'plop-templates/mst/environment.ts.hbs',
        },
        {
          type: 'add',
          path: 'app/mock-state.ts',
          templateFile: 'plop-templates/nst/mock-state.ts.hbs',
        },
        {
          type: 'add',
          path: 'app/services/index.ts',
          templateFile: 'plop-templates/services/index.ts.hbs',
        },
        {
          type: 'add',
          path: 'app/services/authentication/authentication-service.ts',
          templateFile: 'plop-templates/services/authentication-service.ts.hbs',
        },

        {
          type: 'add',
          path: 'app/components/user-status/user-status.props.tsx',
          templateFile: 'plop-templates/mst/components/user-status.props.tsx.hbs',
        },
        {
          type: 'add',
          path: 'app/components/user-status/user-status.component.tsx',
          templateFile: 'plop-templates/mst/components/user-status.component.tsx.hbs',
        },
        {
          type: 'append',
          path: 'app/components/index.ts',
          pattern: `/* PLOP_INJECT_IMPORT */`,
          template: `export * from './user-status/user-status.component'`
        },
        {
          type: 'add',
          force: true,
          path: 'app/screens/login/login.props.tsx',
          templateFile: 'plop-templates/mst/components/login.props.tsx.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/screens/login/login.state.tsx',
          templateFile: 'plop-templates/mst/components/login.state.tsx.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/screens/login/login.screen.tsx',
          templateFile: 'plop-templates/mst/components/login.screen.tsx.hbs',
        },

      ];
      if (data.includeApp=='yes') {
        actions.push({
          type: 'add',
          force: true,
          path: 'app/app.tsx',
          templateFile: 'plop-templates/mst/app.tsx',
        })  
      }
      return actions;
    }
  })


  plop.setGenerator('component', {
    description: 'Create a  component',
    // User input prompts provided as arguments to the template
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?'
      },
      {
        type: 'checkbox',
        name: 'componentType',
        message: 'What type of component would you like?',
        choices: [
          { name: "An Mobx State Tree ready component - a stateful class component with the root tree injected via props", value: "mst" },
          { name: "A standard component - a stateful class component with full lifecycle hooks", value: "component" },
          { name: "A function component - a simple component that just returns a render output and has no state", value: "fc"}
        ]
      },
    ],
    actions: (data) => {
      let actions = [
        {
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.styles.tsx',
          templateFile: 'plop-templates/ComponentStyles.tsx.hbs',
        },
        {
          type: 'append',
          path: 'app/components/index.ts',
          pattern: `/* PLOP_INJECT_IMPORT */`,
          template: `export * from './{{camelCase name}}/{{camelCase name}}.component'`
        },
      ];
      if (data.componentType=='fc') {
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
          templateFile: 'plop-templates/FunctionComponent.tsx.hbs',
        })  
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.props.tsx',
          templateFile: 'plop-templates/ComponentProps.tsx.hbs',
        })
      } else if (data.componentType=="component") {
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
          templateFile: 'plop-templates/Component.tsx.hbs',
        })  
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.props.tsx',
          templateFile: 'plop-templates/ComponentProps.tsx.hbs',
        })
        // Add a state object definition
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.state.tsx',
          templateFile: 'plop-templates/ComponentState.tsx.hbs',
        })  
      } else{
        // MST aware component
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
          templateFile: 'plop-templates/Component-mst.tsx.hbs',
        })  
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.props.tsx',
          templateFile: 'plop-templates/ComponentProps-mst.tsx.hbs',
        })
        // Add a state object definition
        actions.push({
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.state.tsx',
          templateFile: 'plop-templates/ComponentState.tsx.hbs',
        })  
      }
      return actions;
    }
  })


  // Legacy map for creating an FC
    plop.setGenerator('simplecomponent', {
      description: 'Create a simple function component',
      // User input prompts provided as arguments to the template
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What is your component name?'
        },
      ],
      actions: [
        {
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.component.tsx',
          templateFile: 'plop-templates/FunctionComponent.tsx.hbs',
        },
        {
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.props.tsx',
          templateFile: 'plop-templates/ComponentProps.tsx.hbs',
        },
        {
          type: 'add',
          path: 'app/components/{{camelCase name}}/{{camelCase name}}.styles.tsx',
          templateFile: 'plop-templates/ComponentStyles.tsx.hbs',
        },
        {
          type: 'append',
          path: 'app/components/index.ts',
          pattern: `/* PLOP_INJECT_IMPORT */`,
          template: `export * from './{{camelCase name}}/{{camelCase name}}.component'`
        },
      ],
    });

    plop.setGenerator('screen', {
      description: 'Create a new application screen',
      prompts: [
        {
          type: 'input',
          name: 'name',
          message: 'What is your screen name?'
        },
      ],
      actions: [
        {
          type: 'add',
          path: 'app/screens/{{camelCase name}}/{{camelCase name}}.screen.tsx',
          templateFile: 'plop-templates/Screen.tsx.hbs',
        },
        {
          type: 'add',
          path: 'app/screens/{{camelCase name}}/{{camelCase name}}.props.tsx',
          templateFile: 'plop-templates/ScreenProps.tsx.hbs',
        },
        {
          type: 'add',
          path: 'app/screens/{{camelCase name}}/{{camelCase name}}.styles.tsx',
          templateFile: 'plop-templates/ScreenStyles.tsx.hbs',
        },
        {
          type: 'append',
          path: 'app/screens/index.ts',
          pattern: `/* PLOP_INJECT_IMPORT */`,
          template: `export * from './{{camelCase name}}/{{camelCase name}}.screen'`
        },
        {
          type: 'append',
          path: 'app/navigation/route-definitions.tsx',
          pattern: `/* PLOP_INJECT_IMPORT_ROUTES */`,
          templateFile: 'plop-templates/NewRoute.tsx.hbs',
        },
        {
          type: 'append',
          path: 'app/navigation/route-definitions.tsx',
          pattern: `/* PLOP_INJECT_IMPORT */`,
          template: `    {{pascalCase name}}Screen, `,
        },
 
      ],
    });

    plop.setGenerator('modelupdate', {
      description: 'Update the data tier with environment support and service examples',
      prompts: [],
      actions: [
        {
          type: 'add',
          force: true,
          path: 'app/models/setup.ts',
          templateFile: 'plop-templates/mst/setup.ts',
        },
        {
          type: 'add',
          force: true,
          path: 'app/models/environment.ts',
          templateFile: 'plop-templates/mst/environment.ts.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/mock-state.ts',
          templateFile: 'plop-templates/nst/mock-state.ts.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/services/index.ts',
          templateFile: 'plop-templates/services/index.ts.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/services/authentication/authentication-service.ts',
          templateFile: 'plop-templates/services/authentication-service.ts.hbs',
        },
        {
          type: 'add',
          force: true,
          path: 'app/app.tsx',
          templateFile: 'plop-templates/mst/app.tsx',
        },
      ],
    });

  };