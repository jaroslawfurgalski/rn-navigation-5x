import { types, applySnapshot } from 'mobx-state-tree'

enum JobStatus {
    Received = 1,
    Acknowledged,
    Accepted,
    // ... other statuses
    Complete,
}

const JobModel = types.model('Job', {
    id: types.identifier,
    customerName: types.string,
    dueTime: types.maybe(types.Date),
    /*
    Add required properties of a job, for instance stops, locations, whatever
    */
    status: types.number,
}).actions(self => {
    function setStatus(newStatus: JobStatus) {
        applySnapshot(self, {
            ...self, 
            status: newStatus
        })
    }
    return { setStatus }
})

export { JobModel, JobStatus }