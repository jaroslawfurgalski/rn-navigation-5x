import { types } from 'mobx-state-tree'
import { ApplicationModel } from './application-model'

const RootModel = types.model('Root', {
    application: ApplicationModel
})

export { RootModel }