import { types } from 'mobx-state-tree'

const UserModel = types.model('User', {
    id: types.identifier,
    name: types.string,
})

export { UserModel }