import { types } from 'mobx-state-tree'
import { JobModel } from './job-model'

const JobsModel = types.model('Jobs', {
    sourceProvider: types.string,
    activeJobId: types.identifier,
    items: types.array(JobModel)
}).views(self => ({
    // Example of how to implement a view, in this case retrieve the active job
    get activeJob() {
        if (!self.activeJobId || !self.items) {
            return undefined
        }
        return self.items.find(j => j.id == self.activeJobId)
    }
}))

export { JobsModel }