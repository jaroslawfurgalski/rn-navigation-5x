import { Dimensions } from "react-native"
import { color } from "./color"
import { controlPresets, defaultStyles, ColorScheme } from "./style"

export const theme = {
    color: color,
    defaults: defaultStyles,
    controlPresets: controlPresets,
    screen: {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height
    },
    getColorByScheme(scheme: ColorScheme) {
      return scheme ? color[scheme] : color["primary"]
    }
}

/*
This theme is used to seed for material ui, maps in logical equivalents from our more generic theme colors
*/
export const uiTheme = {
    palette: {
        primaryColor: color.primary,
        secondaryColor: color.secondary,
    },
    toolbar: {
      container: {
        height: 50,
      },
    },  
    headerRight: {
      container: {
        marginRight: 10,
        width: 30,
        height: 30,
        borderRadius: 20,
        backgroundColor: '#f5f5dc',
      },
      text: {
        fontSize: 18,
        fontFamily: 'OpenSans-SemiBold',
        textAlign: 'center',
        color: 'black',
      },
    },
}

export { WallpaperPreset, ScreenPreset, TextblockPreset, ControlTypesWithPresets, ColorScheme } from "./style"
