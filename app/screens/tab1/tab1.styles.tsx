import { StyleSheet } from 'react-native';
  
/*
Styles purely for use by this screen. If styles are globally effectual, use "themes" instead
*/

export const Tab1ScreenStyles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    titleText: {
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 22,
        textAlign: 'center'
    }
});
