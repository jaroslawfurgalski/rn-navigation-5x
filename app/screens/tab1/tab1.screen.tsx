import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-material-ui';
import {Tab1Props} from './tab1.props';
import {Wallpaper, Screen, Section, Textblock} from '../../components';
import {Tab1ScreenStyles} from './tab1.styles';
import {defaultStyles} from '../../theme/style';
import SyntaxHighlighter from 'react-native-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/styles/hljs';

export const Tab1Screen: React.FC<Tab1Props> = props => {
  const submit = () => {
    // TODO Add the behaviour to implement onSumit if required
    props.navigation.navigate('Login');
  };


    const codeString = '(num) => num + 1';

  return (
    <Screen navigation={props.navigation}>
      <Section>
        <Textblock style={defaultStyles.tabTitle}>Tab 1</Textblock>
        <View style={{paddingTop: 10, paddingBottom: 10}}> 
        <Textblock style={defaultStyles.smallTitle}>We can set top level screen options and automatically apply them to every single 'Screen' within the app</Textblock>
        </View>
        <Image 
            source={require('../../assets/screenOptions.jpg')}/>
      </Section>
    </Screen>
  );
};
