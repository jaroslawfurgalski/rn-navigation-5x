import React from 'react';
import {Image, Alert} from 'react-native';
import {Button} from 'react-native-material-ui';
import {HomeProps} from './home.props';
import {Wallpaper, Screen, Section, Textblock} from '../../components';
import {defaultStyles} from '../../theme/style';
import {CarouselComponent} from '../../components/carousel/carousel.component';
import { getActiveChildNavigationOptions } from 'react-navigation';
import { uiTheme } from '../../theme';
/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/
export class HomeScreen extends React.Component<HomeProps> {

  submit = () => {
    this.props.navigation.navigate('Tabs');
  };

  openAlert = () => {
    Alert.alert('Test Header');
  };

  render() {
    this.props.navigation.setOptions({
      title: 'TEST',
      headerRight: () => (
        <Button 
                style={uiTheme.headerRight}
                raised
                text="Test"
                upperCase={false}
                onPress={this.openAlert}
              />
    )})
    const { navigate } = this.props
    return (
      <>
        <Screen navigation={this.props.navigation}>
          <Section>
            <Section style={{alignItems: 'center', paddingTop: 50, flex: 1}}>
              <Textblock style={defaultStyles.generalTitle}>Welcome</Textblock>
              <Textblock style={defaultStyles.generalSubTitle}>
                In this POC app you'll find various usage examples of the
                following:
              </Textblock>
            </Section>

            <Section style={{height: 320}}>
              <CarouselComponent />
            </Section>

            <Section style={{justifyContent: 'center', flex: 1}}>
              <Button
                text="Continue"
                raised
                primary
                onPress={this.submit}
                icon="play-arrow"></Button>
            </Section>
          </Section>
        </Screen>
      </>
    );
  }
}
