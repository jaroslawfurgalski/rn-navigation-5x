import React from 'react';
import {Image} from 'react-native';
import {Button} from 'react-native-material-ui';
import {Tab2Props} from './tab2.props';
import {Wallpaper, Screen, Section, Textblock} from '../../components';
import {Tab2Styles} from './tab2.styles';
import {defaultStyles} from '../../theme/style';

export class Tab2Screen extends React.Component<Tab2Props> {
  render() {
    return (
      <Screen navigation={this.props.navigation}>
        <Section>
          <Textblock style={defaultStyles.tabTitle}>Tab 2</Textblock>
          <Button
            text="Update the page title"
            raised
            primary
            onPress={() =>
              this.props.navigation.setOptions({title: 'Updated!'})
            }
            icon="play-arrow"></Button>
        </Section>
      </Screen>
    );
  }
}
