import React from "react"
import { KeyboardAvoidingView, View } from "react-native"
import { Button } from "react-native-material-ui"
import { JobsProps } from "./jobs.props"
import { Screen, Section, Joblist, Textblock, JobStatus } from "../../components"

export const JobsScreen: React.FC<JobsProps> = (props) => {
    const submit = () => { 
        props.navigation.navigate("Login")
    }
    return (
        <Screen>
            <KeyboardAvoidingView>
                <Section>
                    <View><Textblock>Available jobs</Textblock></View>
                    <Joblist></Joblist>
                </Section>
                <Section>
                    <View><Textblock>Active job</Textblock></View>
                    <JobStatus></JobStatus>
                </Section>
                <Section>
                    <Button text="Logout" raised primary onPress={submit} icon="done"></Button>
                </Section>
            </KeyboardAvoidingView>
        </Screen>
    )
}
