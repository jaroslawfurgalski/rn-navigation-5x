import React from "react"
import { Image } from "react-native"
import { Button } from "react-native-material-ui"
import { FeedProps } from "./feed.props"
import { Wallpaper, Screen, Section, Textblock } from "../../components"
import { FeedStyles } from "./feed.styles"

export const FeedScreen: React.FC<FeedProps> = (props) => {
    const submit = () => { 
        // TODO Add the behaviour to implement onSumit if required
        props.navigation.navigate("Login")
    }

    // Any local styles are available via the FeedStyles reference, defined in ./feed.styles

    const logo = require("../../assets/logo.png")
    return (
        <>
        <Wallpaper />
        <Screen>
            <Section>
                <Image source={logo}></Image>
                <Textblock preset="header">Screen "Feed"</Textblock>
            </Section>
            <Section>
                <Button text="Submit" raised primary onPress={submit} icon="done">
                </Button>
            </Section>
        </Screen>
    </>
    )
}