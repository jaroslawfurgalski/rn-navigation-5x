import React from 'react';
import {Image} from 'react-native';
import {Button} from 'react-native-material-ui';
import {Tab3Props} from './tab3.props';
import {Wallpaper, Screen, Section, Textblock, Profile} from '../../components';
import {Tab3Styles} from './tab3.styles';
import {defaultStyles} from '../../theme/style';
import { TopTabs } from '../../navigation';

export class Tab3Screen extends React.Component<Tab3Props> {
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
  
    return {
      title: `${state.params && state.params.title ? state.params.title : 'BASE TITLE'}`,
    };
  };
  

constructor(props) {
  super(props);
}


  render() {
    const {navigate} = this.props.navigation;
    this.props.navigation.setParams
    return (
      <Screen navigation={this.props.navigation} >
        <Section>
          <Textblock style={defaultStyles.tabTitle}>Tab 3</Textblock>
          <Button
            text="Open Drawer"
            raised
            primary
            onPress={() =>
              this.props.navigation.toggleDrawer()
            }
            icon="play-arrow"></Button>
        </Section>
      </Screen>
    );
  }
}
