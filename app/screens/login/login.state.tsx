export type LoginState = {
    isDialogVisible: Boolean,
    dialogMsg?: String,
    username: string,
    password: string
}
