import * as React from 'react';
import { KeyboardAvoidingView, TextInput } from "react-native"
import { Button, Snackbar } from "react-native-material-ui"
import { LoginProps } from "./login.props"
import { LoginState } from "./login.state"
import { theme } from "../../theme"
import { Screen, Section, Textblock } from "../../components"
import { inject, observer } from 'mobx-react'
import { getEnv } from 'mobx-state-tree'
import { AuthenticationService, AuthenticationRequest } from "../../services"


/*
If your screen/component needs to be state aware, uss a class component instead of a FunctionComponent (FC)
*/

@inject('rootTree')
@observer
export class LoginScreen extends React.Component<LoginProps, LoginState> {
    state: {
        username: '',
        password: '',
        isDialogVisible: false,
        dialogMsg: ''
    }

    constructor(props: LoginProps) {
        super(props)
    }

    submit = async() => { 
        const authenticationService: AuthenticationService = getEnv(this.props.rootTree).authenticationService
        const request: AuthenticationRequest = {
            username: this.state.username,
            password: this.state.password
        }
        const result = await authenticationService.login(request)
        if (!result) {
            this.props.rootTree.application.error('Incorrect credentials')
            return;
        }
        this.setState({ isDialogVisible: true, dialogMsg: result ? ('Welcome, ' + result.displayName) : 'Login failed' })
        this.props.rootTree.application.loggedIn(result.id, result.username)}


    onChangeUsername = (username) => this.setState({ username })

    onChangePassword = (password) => this.setState({ password })

    render() {
        const { rootTree } = this.props

        if (!rootTree) {
            return null
        }
        const visible = (this.state) ? this.state.isDialogVisible : false
        const msg = (this.state) ? this.state.dialogMsg : ''

        return (
            <>
            <Snackbar visible={visible} message={msg} onRequestClose={() => this.setState({ isDialogVisible: false })} />
            <Screen navigation={this.props.navigation}>
                <KeyboardAvoidingView>
                    <Section>
                        <Textblock>Username</Textblock>
                        <TextInput onChangeText={this.onChangeUsername} style={theme.defaults.textInput}></TextInput>
                        <Textblock>Password</Textblock>
                        <TextInput onChangeText={this.onChangePassword} style={theme.defaults.textInput}></TextInput>
                    </Section>
                    <Section>
                        <Button text="Login" raised primary onPress={this.submit} icon="done">
                        </Button>
                    </Section>
                </KeyboardAvoidingView>
            </Screen>
            </>
        )
    }    
}