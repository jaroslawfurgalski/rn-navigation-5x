import { NavigationInjectedProps } from "react-navigation"
import { Root } from "../../models"

export interface LoginProps extends NavigationInjectedProps<{}> { 
    rootTree?: Root,
}