import React from "react"
import { Image } from "react-native"
import { Button } from "react-native-material-ui"
import { SettingsProps } from "./settings.props"
import { Wallpaper, Screen, Section, Textblock } from "../../components"
import { SettingsStyles } from "./settings.styles"

export const SettingsScreen: React.FC<SettingsProps> = (props) => {
    const submit = () => { 
        // TODO Add the behaviour to implement onSumit if required
        props.navigation.navigate("Login")
    }

    // Any local styles are available via the SettingsStyles reference, defined in ./settings.styles

    const logo = require("../../assets/logo.png")
    return (
        <>
        <Wallpaper />
        <Screen>
            <Section>
                <Image source={logo}></Image>
                <Textblock preset="header">Screen "Settings"</Textblock>
            </Section>
            <Section>
                <Button text="Submit" raised primary onPress={submit} icon="done">
                </Button>
            </Section>
        </Screen>
    </>
    )
}