import { NavigationInjectedProps } from "react-navigation"

export interface SettingsProps extends NavigationInjectedProps<{}> { }