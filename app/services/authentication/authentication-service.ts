import { 
    AuthenticationService,
    AuthenticationRequest,
    AuthenticatedUser
} from "../index"

export class MockAuthenticatedUser implements AuthenticatedUser {
    id: string
    username: string
    displayName: string
}

export class MockAuthenticationService implements AuthenticationService {
    token: string;

    async login(authenticationRequest: AuthenticationRequest): Promise<AuthenticatedUser> {
        if (!authenticationRequest.username || authenticationRequest.password != 'password') {
            return await undefined
        }

        const user: MockAuthenticatedUser = {
            id: '99',
            username: authenticationRequest.username,
            displayName: authenticationRequest.username
        }
        this.token = "mock-token"
        return await user
    }    
    async logout(): Promise<boolean> {
        this.token = undefined
        return await false
    }
    async isLoggedIn(): Promise<boolean> {
        return await (this.token != undefined)
    }
}