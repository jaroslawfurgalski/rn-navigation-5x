import { MessageBroker } from "..";
import { Root } from "../../models";

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

export class CommsManager {
    // TODO Use the constructor to bring in anything that the comms manager needs to function
    // for instance a storage provider
    constructor(messageBrokers: Array<MessageBroker>) {
        this.messageBrokers = messageBrokers
    }

    // We initialise with the rootStore instance because this will write to state via actions, which
    // will then trigger activity maintained within the Screen wrapper component
    async initialise(rootStore: Root) {
        this.rootStore = rootStore

        asyncForEach(this.messageBrokers, async (broker) => {
            await broker.setup((message: any) => {
                rootStore.application.newjob(message)
            }, (error, msg) => {
                rootStore.application.error(msg)
            })
        })
        console.log('commsManager initialised:')
    }

    messageBrokers: Array<MessageBroker>
    rootStore: Root
}