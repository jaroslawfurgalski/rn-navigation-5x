import { MessageBroker } from ".."
import { MessageBrokerPayloadCallback, MessageBrokerErrorCallback } from "../../services"
import { PermissionsAndroid } from "react-native";
import SmsListener from "react-native-android-sms-listener"
import { SmsAgent } from "../agents/sms-agent"

export class SmsMessageBroker implements MessageBroker {

    async setup(onPayload: MessageBrokerPayloadCallback, onError: MessageBrokerErrorCallback): Promise<Boolean> {
        await requestReadSmsPermission()

        SmsListener.addListener(message => {
            const agent = new SmsAgent()
            try {
                const _job = agent.validateAndTransform(message.body)
                const job = agent.translateToInternalFormat(_job)
                onPayload(job)
            } catch(e) {
                onError(agent.errors, "An error occured translating an SMS into a Job")
            }
        })
        return true
    }

    send(payload: any): boolean {
        return false
    }
    
    receive(): any {
        return undefined
    }
}



async function requestReadSmsPermission() {
  try {
    var granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_SMS,
      {
        title: 'Auto Verification OTP',
        message: 'need access to read sms, to verify OTP',
        buttonPositive: "Ok"
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('sms read permissions granted', granted);
      granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
        {
          title: 'Receive SMS',
          message: 'Need access to receive sms, to verify OTP',
          buttonPositive: "Ok"
      },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('RECEIVE_SMS permissions granted', granted);
      } else {
        console.log('RECEIVE_SMS permissions denied');
      }
    } else {
      console.log('sms read permissions denied');
    }
  } catch (err) {
    console.log(err);
  }
}
