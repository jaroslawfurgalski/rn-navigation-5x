import { Job as InternalJob } from "../../models"
import { JobStatus } from "../../models/job-model";

export interface Job {
    bookingTime?: string,
    jobId: string,
    number?: string,
    passengers?: number,
    pickupTime: string,
    prebook: boolean,
    price?: Price,
    stops: Stop[]
}
export interface Price {
    totalCharged: number,
}
export interface Stop {
    address: Address,
    class?: StopClass,
    passengers?: Passenger[]
}
export enum StopClass {
    DROPOFF = "DROPOFF",
    PICKUP = "PICKUP"
}
export interface Address {
}
export interface Passenger {
    name: string,
    telephone: string
}
export enum Separators {
    ELEMENTS = ";",
    PROPERTIES = ":"
}
  
export class JobAgent {
    public errors: Error[] = [];
    // TODO: Rules to confirm we have a viable job object to live here.
    //
    // tslint:disable-next-line no-any
    protected validateJobObject(supposedJob: any): boolean {
      return supposedJob &&
        (supposedJob.stops && Array.isArray(supposedJob.stops) && supposedJob.stops.length > 0) &&
        supposedJob.pickupTime;
    }
    public translateToInternalFormat(job: Job): InternalJob {
        const result = {
            id: job.jobId,
            customerName: this.customerName(job.stops),
            dueTime: (new Date()).getTime(),
            status: JobStatus.Received,
        }
        return result as InternalJob
    }
    private customerName(stops: Array<Stop>) {
        const pickup: Stop = stops ? (stops.find(s => s.class == StopClass.PICKUP)) : undefined
        return pickup && pickup.passengers ? pickup.passengers[0].name : "(no customer)"
    }
  }
  