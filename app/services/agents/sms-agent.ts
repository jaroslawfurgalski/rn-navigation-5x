import { 
    JobAgent,
    Separators,
    Job,
    StopClass
} from "./job-agent"

export class SmsAgent extends JobAgent {
    // Ensure that the message we've been supplied meets at least the minimum requirements -- i.e. that it is a string with
    // some separators in.
    //
    // This can obviously be expanded upon as required.
    //
    // tslint:disable-next-line no-any
    private validateSmsMessage(payload: any): boolean {
      const elementSeparatorLocation: RegExpMatchArray = payload.match(new RegExp(Separators.ELEMENTS));
      if (!elementSeparatorLocation || elementSeparatorLocation.length < 1) {
        this.errors.push(new Error("No valid element separators found."));
        return false;
      }
      return true;
    }
    // Based on the input object, create an object we can work with.
    //
    // tslint:disable-next-line no-any
    private createGenericObjectFromSms(payload: any): any {
      const elements: string[] = payload.split(Separators.ELEMENTS);
      // tslint:disable-next-line no-any
      return elements.reduce((acc: any, curr: string) => {
        const propertySeparatorLocation: RegExpMatchArray = curr.match(new RegExp(Separators.PROPERTIES));
        if (!propertySeparatorLocation || propertySeparatorLocation.length < 1) {
          return acc;
        }
        const [prop, value]: string[] = curr.split(Separators.PROPERTIES);
        acc[prop.trim().toLowerCase()] = value.trim();
        return acc;
      }, {});
    }
    // tslint:disable-next-line no-any
    private createJobFromCtSms(genericObject: { [key: string]: string }): Job {
      // Destructuring here will allow us to quickly re-map properties as required to an alias for use further down.
      // NB: Incoming properties (on the left) are all lowercased by default.
      const {
        id: jobId,
        pickup: pickupAddress,
        drop: dropAddress,
        bookedat: bookingTime,
        passengers: passengers,
        customername: passengerName,
        passengertelephone: passengerTelephone,
        pickuptime: pickupTime,
        type: jobType,
        price: price
      } = genericObject;
      const job: Job = {
        jobId: jobId,
        bookingTime: bookingTime,
        passengers: parseInt(passengers) || 0,
        pickupTime: pickupTime || "ASAP",
        prebook: (jobType || "").toUpperCase() === "PREBOOK" ? true : false,
        price: {
          totalCharged: parseInt(price) || 0
        },
        stops: []
      };
      if (pickupAddress) {
        job.stops.push(
          {
            class: StopClass.PICKUP,
            address: pickupAddress,
            passengers: [
              {
                name: passengerName,
                telephone: passengerTelephone
              }
            ]
          });
      }
      if (dropAddress) {
        job.stops.push(
          {
            class: StopClass.DROPOFF,
            address: dropAddress
          });
      }
      return job;
    }
    // tslint:disable-next-line no-any
    private createJobFromMuleSms(genericObject: { [key: string]: string }): Job {
      // Destructuring here will allow us to quickly re-map properties as required.
      // NB: Incoming properties (on the left) are all lowercased by default.
      const {
        job: jobId,
        pickup: pickupAddress,
        drop: dropAddress,
        pickuptime: pickupTime,
        type: jobType,
        pass: pass
      } = genericObject;
      return {
        jobId: jobId,
        pickupTime: pickupTime || "ASAP",
        prebook: (jobType || "").toUpperCase() === "PREBOOK" ? true : false,
        stops: [
          {
            class: StopClass.PICKUP,
            address: pickupAddress,
            passengers: [
                {
                    name: pass,
                    telephone: ""
                }                    
            ]
          },
          {
            class: StopClass.DROPOFF,
            address: dropAddress
          }
        ]
      };
    }
    // tslint:disable-next-line no-any
    public validateAndTransform(payload: any): Job {
      const DEFAULT_SOURCE: string = "mule";
      if (!this.validateSmsMessage(payload)) {
        return undefined;
      }
      const genericObject: object = this.createGenericObjectFromSms(payload);
      // Alternate SMS sources would be added here:
      let createJobObjectFunction: Function;
      switch ((genericObject["source"] || DEFAULT_SOURCE).toUpperCase()) {
        case "CT":
          createJobObjectFunction = this.createJobFromCtSms;
          break;
        case "MULE":
          createJobObjectFunction = this.createJobFromMuleSms;
          break;
      }
      const supposedJob: Job = createJobObjectFunction.bind(this, genericObject)();
      if (!this.validateJobObject(supposedJob)) {
        this.errors.push(new Error("Could not create a valid job object."));
        return undefined;
      }
      return supposedJob;
    }
  }
  