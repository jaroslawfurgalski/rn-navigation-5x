import { Job } from "../models"
import { CommsManager } from "./brokers/comms-manager"

/*
Our services should implement interfaces as it will make it viable for us
to test via the abstration against mocked versions etc.
*/
export interface AuthenticationRequest {
    username: string
    password: string
}
export interface AuthenticatedUser {
    id: string
    username: string
    displayName: string
}
type AsyncLoginFunction = (authenticationRequest: AuthenticationRequest) => Promise<AuthenticatedUser>
type AsyncBooleanFunction = () => Promise<boolean>

export interface AuthenticationService {
    login: AsyncLoginFunction
    logout: AsyncBooleanFunction
    isLoggedIn: AsyncBooleanFunction
}

type JobTransformFunction = (payload: any) => Job

export interface JobAgent {
    errors?: Array<Error>
    validateAndTransform: JobTransformFunction
}

export type MessageBrokerPayloadCallback = (message: any) => any
export type MessageBrokerErrorCallback = (error: any, message: String) => any

type MessageBrokerSetupFunction = (onPayload: MessageBrokerPayloadCallback, onError: MessageBrokerErrorCallback) => Promise<Boolean>
type MessageSendFunction = (message: any) => Boolean
type MessageReceiveFunction = () => any

/*
  A message broker deals with a communications channel and will be able to send a payload on
  that channel and receive and signal a message inbound to that channel
*/
export interface MessageBroker {
    setup: MessageBrokerSetupFunction
    send: MessageSendFunction
    receive: MessageReceiveFunction
}

export class EnvironmentConfig {
    constructor(options: any, authenticationService: AuthenticationService) {
        this.options = options
        this.authenticationService = authenticationService
    }
    options: any
    authenticationService: AuthenticationService
    commsManager: CommsManager
}