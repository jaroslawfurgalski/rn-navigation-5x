import { ViewStyle } from "react-native"
import { Root } from "../../models"

export interface CarouselProps {
  style?: ViewStyle,
  rootTree?: Root,
}
