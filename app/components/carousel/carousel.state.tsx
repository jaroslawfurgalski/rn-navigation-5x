/*
State definition for use within an associated stateful component
*/

interface Item {
    title: string,
    component: any
}
export type State = {
    ImageStack: Item,
}
