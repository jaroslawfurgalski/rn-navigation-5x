import React from "react"
import { View, Image } from "react-native"
import { CarouselProps } from "./carousel.props"
import { CarouselStyles } from "./carousel.styles"
import { inject, observer } from 'mobx-react'
import { Textblock } from "../textblock/textblock.component"
import { Joblist } from "../joblist/joblist.component"
import Carousel from 'react-native-snap-carousel';
import { defaultStyles } from "../../theme/style"

@inject('rootTree')
@observer
export class CarouselComponent extends React.Component<CarouselProps> {
    state = {
      activeSlide: 0,
       ImageStack: [
        {
           component: <View><Image style={{resizeMode: 'contain'}}
           source={require('../../assets/stackNav.jpg')}/></View>,
           title: <Textblock style={defaultStyles.smallTitle}>New Stack Navigator</Textblock>
        },
          {
            component: <View><Image style={{resizeMode: 'contain'}}
            source={require('../../assets/drawNav.jpg')}/></View>,
            title: <Textblock style={defaultStyles.smallTitle}>New Draw Navigator</Textblock>
           },
           {
            component: <View><Image style={{resizeMode: 'contain'}}
            source={require('../../assets/topTabNav.jpg')}/></View>,
            title: <Textblock style={defaultStyles.smallTitle}>New Top Bar Navigator</Textblock>
           },
          ]
    }

    // Component lifecycle
    constructor(props: CarouselProps) {
        super(props)
    }

    _renderItem = ({item, index}) => {
        return (
            <View style={CarouselStyles.container}>
               <View>{item.component}</View>
              <Textblock>{item.title}</Textblock>
            </View>
        );
    }

    render () {
        return (
            <Carousel
              layout={'stack'} layoutCardOffset={`18`}
              data={this.state.ImageStack}
              renderItem={this._renderItem}
              loop={true}
              sliderWidth={320}
              itemWidth={300}
              slideStyle={{ flex: 1 }}
            />
        );
    }
}

