import { StyleSheet } from 'react-native';
  
/*
Styles purely for use by this component. If styles are globally effectual, use "themes" instead
*/

export const CarouselStyles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});
