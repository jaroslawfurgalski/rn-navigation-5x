/*
State definition for use within an associated stateful component
*/

export type CustomHeaderState = {
    value: number
}
