import { ViewStyle } from "react-native"
import { Root } from "../../models"
import { NavigationInjectedProps } from "react-navigation"

export interface CustomerHeader extends NavigationInjectedProps<{}> {
  style?: ViewStyle,
  rootTree?: Root,
}
