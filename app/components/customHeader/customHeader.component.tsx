import React from "react"
import { View, Text } from "react-native"
import { Textblock } from ".."
import { theme } from "../../theme"
import { CustomHeaderProps } from "./customHeader.props"
import { CustomHeaderStyles } from "./customHeader.styles"
import { CustomHeaderState } from "./customHeader.state"
import { inject, observer } from 'mobx-react'
import { NavigationContainer } from "@react-navigation/native"

/*
Note these two decorators. In conjunction with including the rootTree property in the props definition,
this will make rootTree available within this.props.rootTree, from which you can access whatever you
want within your model tree
*/
@inject('rootTree')
@observer
export class CustomHeader extends React.Component<CustomHeaderProps, CustomHeaderState> {
    static navigationOptions = {
        headerTitle: 'This will not show at all'
     }

    state = {
        value: 1
    }

    // Component lifecycle
    constructor(props: CustomHeaderProps) {
        super(props)
    }

    componentDidMount() {
        this.props.navigation.setParams({
            myTitle: this.props.myTitle
           })
    }
    //TODO SET DYNAMIC TITLE


    // Rendering
    render() {
        const { rootTree, style } = this.props

        if (!rootTree) {
            return null
        }
        return (
          <View style={style}>
              {this.props.navigation.setParams}
         </View>
        )
    }    
}
