import { StyleSheet } from 'react-native';
  
/*
Styles purely for use by this component. If styles are globally effectual, use "themes" instead
*/

export const CustomHeaderStyles = StyleSheet.create({
        headerStyle: {
          backgroundColor: 'orange',
          height: 50
        }
});
