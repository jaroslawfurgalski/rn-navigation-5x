import React from "react"
import { View } from "react-native"
import { Textblock } from ".."
import { JoblistProps } from "./joblist.props"
import { inject, observer } from 'mobx-react'
import { Job } from "../../models"
import { Jobtile } from "../jobtile/jobtile.component"

@inject('rootTree')
@observer
export class Joblist extends React.Component<JoblistProps> {

    constructor(props: JoblistProps) {
        super(props)
    }

    selectJob = (job: Job) => {
        this.props.rootTree.application.jobs.setActiveJob(job)
        if (this.props.onSelectJob) {
            this.props.onSelectJob(job)
        }
    }

    renderJobs(jobs: Array<Job>) {
        if (!jobs || jobs.length == 0) {
            return (
                <View>
                    <Textblock>(no jobs available)</Textblock>
                </View>
            )
        }
        return jobs.map((item) => {
            return (
                <View>
                    <Jobtile job={item} key={item.id} onSelect={this.selectJob}></Jobtile>
                </View>
            )
        })
    }

    render() {
        const { rootTree } = this.props

        if (!rootTree) {
            return null
        }
        const jobs: Array<Job> = rootTree.application.jobs ? (rootTree.application.jobs.items ?? []) : []
        return (
            <View>
                { this.renderJobs(jobs) }                   
            </View>
        )
    }    
}
