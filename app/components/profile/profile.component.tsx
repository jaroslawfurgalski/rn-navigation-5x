import React from "react"
import { View } from "react-native"
import { Textblock } from ".."
import { theme } from "../../theme"
import { ProfileProps } from "./profile.props"
import { ProfileStyles } from "./profile.styles"
import { ProfileState } from "./profile.state"
import { inject, observer } from 'mobx-react'

/*
Note these two decorators. In conjunction with including the rootTree property in the props definition,
this will make rootTree available within this.props.rootTree, from which you can access whatever you
want within your model tree
*/
@inject('rootTree')
@observer
export class Profile extends React.Component<ProfileProps, ProfileState> {

    // Look at https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html for best practice on lifecycle handling and key concepts
    // Initialise state
    state = {
        value: 1
    }

    // Component lifecycle
    constructor(props: ProfileProps) {
        super(props)
    }

    componentDidMount() {
    }


    // Rendering
    render() {
        const { rootTree, style } = this.props

        if (!rootTree) {
            return null
        }
        return (
            <View style={style}>
                <Textblock>Last login: {rootTree.application.lastLogin}</Textblock>
                <Textblock>I last updated at {new Date().toLocaleTimeString()}</Textblock>
            </View>
        )
    }    
}
