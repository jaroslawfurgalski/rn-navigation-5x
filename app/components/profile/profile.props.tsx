import { ViewStyle } from "react-native"
import { Root } from "../../models"

export interface ProfileProps {
  style?: ViewStyle,
  rootTree?: Root,
}
