import { ViewStyle } from "react-native"
import { Job } from "../../models"

export interface JobtileProps {
  style?: ViewStyle,
  job?: Job,
  onSelect?: any
}
