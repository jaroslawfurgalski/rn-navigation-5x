import React from "react"
import { View, TouchableOpacity } from "react-native"
import { Textblock } from ".."
import { JobtileProps } from "./jobtile.props"
import { observer } from 'mobx-react'

/*
Here we're demonstrating a component that doesn't have a direct store injection as this is designed to be seeded
with a job entry in the properties. In this case, it only needs the @observer and no @inject is required. Look
in the properties class definition and you'll see the job entry there, then look at the Jobs screen for example usage
*/
@observer
export class Jobtile extends React.Component<JobtileProps> {

    constructor(props: JobtileProps) {
        super(props)
    }

    handlePress = async(e) => { 
        e.preventDefault()
        if (this.props.onSelect) {
            this.props.onSelect(this.props.job)
        }
    }

    // Rendering
    render() {
        const { job, style } = this.props

        if (!job) {
            return null
        }

        const due = job.dueTime ? new Date(job.dueTime).toLocaleTimeString() : '(not set)'
        return (
            <TouchableOpacity onPress={this.handlePress} >
            <View style={style}>
                <Textblock>Customer: {job.customerName}</Textblock>
                <Textblock>Booked for: {due}</Textblock>
            </View>
            </TouchableOpacity>
        )
    }    
}
