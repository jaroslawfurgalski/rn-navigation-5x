import { ViewStyle } from "react-native"
import { Root } from "../../models"

export interface JobStatusProps {
  style?: ViewStyle,
  rootTree?: Root,
}
