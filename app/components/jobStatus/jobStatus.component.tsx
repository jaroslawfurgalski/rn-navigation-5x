import React from "react"
import { View } from "react-native"
import { Textblock, Jobtile } from ".."
import { JobStatusProps } from "./jobStatus.props"
import { inject, observer } from 'mobx-react'

@inject('rootTree')
@observer
export class JobStatus extends React.Component<JobStatusProps> {
    constructor(props: JobStatusProps) {
        super(props)
    }

    render() {
        const { rootTree } = this.props

        if (!rootTree) {
            return null
        }

        const { jobs } = rootTree.application
        const job = jobs ? jobs.activeJob : undefined
        if (!job) {
            return (
                <View>
                    <Textblock>(no active job)</Textblock>
                </View>
            )
        }
        return (
            <View>
                <Jobtile job={job} key={job.id}></Jobtile>
                <Textblock>Current status: {job.status}</Textblock>
            </View>
        )
    }    
}
