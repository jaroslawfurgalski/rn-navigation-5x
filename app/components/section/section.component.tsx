import React from "react"
import { View } from "react-native"
import { theme } from "../../theme"
import { SectionProps } from "./section.props"

/*
A simple wrapper component tp contain a logical section of the page/screen
*/
export const Section: React.FC<SectionProps> = (props) => {
    const style = props.style || { ...theme.defaults.section }
    return (
        <View style={style}>
            {props.children}
        </View>
    )
}
