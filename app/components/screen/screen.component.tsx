import React, { ReactNode } from "react"
import { View } from "react-native"
import { theme} from "../../theme"
import { ScreenProps } from "./screen.props"
import { ScreenState, DialogActionDelegate, NotificationDisplayState } from "./screen.state"
import { inject, observer } from 'mobx-react'
import { onAction } from "mobx-state-tree"
import { Snackbar, Dialog, DialogDefaultActions } from "react-native-material-ui"
import { Textblock } from ".."

/*
A simple wrapper component to host the content for a page/screen
*/
@inject('rootTree')
@observer
export class Screen extends React.Component<ScreenProps, ScreenState> {
    dialogActions: Array<DialogActionDelegate> = []
    statePayload: any

    state: {
        notificationDisplayState: NotificationDisplayState.none,
        dialogMsg: ''
    }

    constructor(props: ScreenProps) {
        super(props)

        onAction(props.rootTree, call => {
            if (call.name === "error") {
                this.setState({ notificationDisplayState: NotificationDisplayState.snackbar, dialogMsg: call.args[0] })

            } else if (call.name === "newjob") {
                // If a job is already active, just give the notification
                if (props.rootTree.application.jobs && props.rootTree.application.jobs.activeJob) {
                    this.setState({ notificationDisplayState: NotificationDisplayState.snackbar, dialogMsg: "A new job has been received and will be available after completing the current job" })    

                } else {
                    this.statePayload = call.args[0]
                    this.dialogActions = [
                        { name: "ok", onAction: this.acceptJob },
                        { name: "cancel" }
                    ]
                    this.setState({ notificationDisplayState: NotificationDisplayState.dialog, dialogMsg: "Would you like to start the new job now?" })    
                }

            }
        })
    }

    acceptJob(jobSnapshot: any, props: ScreenProps) {
        props.rootTree.application.jobs.setActiveJobById(jobSnapshot.id)
        props.navigation.navigate("Jobs")
    }

    dialogActionTapped(e) {
        this.setState({ notificationDisplayState: NotificationDisplayState.none, dialogMsg: "" })
        if (this.dialogActions) {
            const selectedAction = this.dialogActions.find(a => a.name == e)
            if (selectedAction && selectedAction.onAction) {
                // Small delay to allow the setState above to have completed so we don't get
                // a lifecycle exception with us navigating away while that happens
                setTimeout(() => selectedAction.onAction(this.statePayload, this.props), 300)
            }
        }
    }
 
    renderSnackbar(msg: String): ReactNode {
        const timeout: Number = 6000
        setTimeout(() => this.setState({ notificationDisplayState: NotificationDisplayState.none }), timeout)
        return (
            <Snackbar 
                visible={true} 
                message={msg} 
                timeout={timeout} 
                onPress={() => this.setState({ notificationDisplayState: NotificationDisplayState.none })} 
                onRequestClose={() => this.setState({ notificationDisplayState: NotificationDisplayState.none })} 
            />
        )
    }

    renderDialog(msg: String): ReactNode {
        const actions = this.dialogActions.map((a) => { return a.name })

        return (
            <View>
                <Dialog>
                <Dialog.Title><Textblock>New job received</Textblock></Dialog.Title>
                <Dialog.Content>
                <Textblock>
                    {msg}
                </Textblock>
                </Dialog.Content>
                <Dialog.Actions>
                <DialogDefaultActions
                    actions={actions}
                    onActionPress={(e) => this.dialogActionTapped(e) }
                />
                </Dialog.Actions>
            </Dialog>
          </View>
        )
    }

    render() {
        const { rootTree } = this.props
        const style = this.props.style || { ...theme.defaults.screen }

        if (!rootTree) {
            return null
        }
        let displayState: NotificationDisplayState = NotificationDisplayState.none, msg: String = '', child: ReactNode = (<></>)
        if (this.state && this.state.notificationDisplayState) {
            displayState = this.state.notificationDisplayState
        }
        if (this.state && this.state.dialogMsg) {
            msg = this.state.dialogMsg
        }

        switch (displayState) {
            case 1: child = this.renderSnackbar(msg)
                break
            case 2: child = this.renderDialog(msg)
                break
            default:
                break
        }

        return (
            <>
            {child}
            <View style={style}>
                {this.props.children}
            </View>
            </>
        )
    }    

}
