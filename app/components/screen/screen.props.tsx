import { ViewStyle } from "react-native"
import { Root } from "../../models"
import { NavigationInjectedProps } from "react-navigation"

export interface ScreenProps extends NavigationInjectedProps<{}> {
  style?: ViewStyle,
  rootTree?: Root,
}
