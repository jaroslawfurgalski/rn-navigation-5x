import { ScreenProps } from "./screen.props"

type OnAction = (data: any, props: ScreenProps) => void

export enum NotificationDisplayState {
    none,
    snackbar,
    dialog,
}

export type DialogActionDelegate = {
    name: String,
    onAction?: OnAction,
}

export type ScreenState = {
    notificationDisplayState: NotificationDisplayState,
    dialogMsg?: String,
}