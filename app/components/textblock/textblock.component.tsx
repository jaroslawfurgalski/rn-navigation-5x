import React from "react"
import { Text, TextStyle } from "react-native"
import { theme, TextblockPreset, ControlTypesWithPresets } from "../../theme"
import { TextblockProps } from "./textblock.props"

/*
A simple wrapper component tp contain a logical section of the page/screen
*/
export const Textblock: React.FC<TextblockProps> = (props) => {
    const presetControlType: ControlTypesWithPresets = "textblock"
    const defaultPreset: TextblockPreset = "content"
    const style: TextStyle = props.style || { ...theme.controlPresets[presetControlType][props.preset||defaultPreset], ...theme.defaults.textblock, color: theme.getColorByScheme(props.scheme) }
    return (
        <Text style={style}>
            {props.children}
        </Text>
    )
}
