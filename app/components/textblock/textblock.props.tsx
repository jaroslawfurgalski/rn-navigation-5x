import { ViewStyle, TextStyle } from "react-native"
import { TextblockPreset, ColorScheme } from "../../theme"

export interface TextblockProps {
  style?: TextStyle,
  preset?: TextblockPreset,
  scheme?: ColorScheme,
}
