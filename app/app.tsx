import React, { useState, useEffect, Component } from "react"
import {AppRegistry} from "react-native"
import {name as appName} from "../app.json"
import { uiTheme } from "./theme"
import { ThemeContext, getTheme } from "react-native-material-ui"
import { Provider } from 'mobx-react'
import { setupRootStore, Root } from "./models"
import { EnvironmentConfig, MessageBroker } from "./services/index"
import { MockAuthenticationService } from "./services/authentication/authentication-service"
import { MockInitialState } from "./mock-state"
import { CommsManager } from "./services/brokers/comms-manager"
import { SmsMessageBroker } from "./services/brokers/sms-broker"

import {NavigationContainer} from '@react-navigation/native'
import NavigatorContainer from "./navigation/navigation"

/**
 * PW - LIFTED FROM IGNITE/BOWSER
 * Storybook still wants to use ReactNative's AsyncStorage instead of the
 * react-native-community package; this causes a YellowBox warning. This hack
 * points RN's AsyncStorage at the community one, fixing the warning. Here's the
 * Storybook issue about this: https://github.com/storybookjs/storybook/issues/6078
 */
const ReactNative = require("react-native");
Object.defineProperty(ReactNative, "AsyncStorage", {
  get(): any {
    return require("@react-native-community/async-storage").default
  },
})


export const App: React.FC<{}> = () => {
  const initialData = (__DEV__) ? MockInitialState : undefined


  //=======================================================
  /* Generate the CommsManager and assign brokers for it */
  const brokers: Array<MessageBroker> = [
    new SmsMessageBroker()
  ]
  const commsManager = new CommsManager(brokers)
  //=======================================================
  
  const environmentCofig: EnvironmentConfig = {
    options: {

    },
    authenticationService: new MockAuthenticationService,
    commsManager: commsManager
  }

  const [rootStore, setRootStore] = useState<Root | undefined>(undefined) // prettier-ignore
  useEffect(() => {
    setupRootStore(environmentCofig, initialData)
      .then(setRootStore) 
      .catch(e => {
        console.error('assign state fail: ', e)
      })
  }, [])

  if (!rootStore) {
    return null
  }  

  commsManager.initialise(rootStore)

  return (
    <Provider rootTree={rootStore}>
      <ThemeContext.Provider value={getTheme(uiTheme)}>
          <NavigatorContainer />
      </ThemeContext.Provider>
    </Provider>
  );
}

// PW - ALSO LIFTED FROM IGNITE/BOWSER
// Should we show storybook instead of our app?
//
// ⚠️ Leave this as `false` when checking into git.
const SHOW_STORYBOOK = false

let RootComponent = App
if (__DEV__) {
  // PW - UNCOMMENT THIS WHEN STORYBOOK HAS BEEN BROUGHT IN PROPERLY
  //const { StorybookUIRoot } = require("../storybook")
  //if (SHOW_STORYBOOK) RootComponent = StorybookUIRoot
}

AppRegistry.registerComponent(appName, () => RootComponent)

