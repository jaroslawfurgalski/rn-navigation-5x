import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {RootStack, RootDrawer, RootAuth} from './route-definitions';
import React, {Component} from 'react';
import { getEnv } from 'mobx-state-tree'

export const Drawer = createDrawerNavigator();
export const Stack = createStackNavigator();
export const BottomTabs = createBottomTabNavigator();
export const TopTabs = createMaterialTopTabNavigator();
import {inject, observer} from 'mobx-react';
import {NavigationProps} from './NavigationProps';
import { AuthenticationService, AuthenticationRequest } from '../services';

@inject('rootTree')
@observer
export default class NavigatorContainer extends Component<NavigationProps> {
  render() {

    const {rootTree} = this.props;
    if (!rootTree) {
      return null;
    }

    const authenticationService: AuthenticationService = getEnv(this.props.rootTree).authenticationService
    const isSignedIn = authenticationService.isLoggedIn()
    console.log(isSignedIn)

    return (
      <NavigationContainer>
        {isSignedIn ? (
       <RootStack />  ) : ( <RootAuth />)}

      </NavigationContainer>
    );
  }
}
