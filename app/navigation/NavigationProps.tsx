import { NavigationInjectedProps } from "react-navigation"
import { Root } from "../models";

export interface NavigationProps extends NavigationInjectedProps<{}> {
  rootTree?: Root
}
