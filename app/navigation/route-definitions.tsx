import React, {Component} from 'react';
import {LoginScreen, HomeScreen} from '../screens';

import {
  /* PLOP_INJECT_IMPORT */
  LoadingScreen, 
  Tab3Screen,
  Tab2Screen,
  Tab1Screen,
  SettingsScreen,
} from '../screens';

import {Drawer, Stack, BottomTabs, TopTabs} from './navigation';
import {CustomHeader, Wallpaper} from '../components';

import {CustomHeaderStyles} from '../components/customHeader/customHeader.styles';
import { NavigationActions } from 'react-navigation';


export function RootStack() {
  return (
    <Stack.Navigator
      headerMode="screen"
      initialRouteName="Home"
      screenOptions={{
        headerStyle: {
          backgroundColor: 'orange'
        },
        headerTitleAlign: 'center',
        headerBackTitleVisible: true
      }}
      >
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Tabs" children={RootTopTabs} />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{title: 'Home Screen'}}
      />
    </Stack.Navigator>
  );
}

export function RootAuth() {
  return (
    <Stack.Navigator
      headerMode="screen"
      initialRouteName="Login">
      <Stack.Screen name="Login" component={LoginScreen}/>
    </Stack.Navigator>
  );
}


export function RootTopTabs() {
  return (
    <TopTabs.Navigator>
      <TopTabs.Screen
        name="Tab1"
        options={{title: 'Options'}}
        component={Tab1Screen}
      />
      <TopTabs.Screen
        name="Tab2"
        options={{title: 'Forms'}}
        component={Tab2Screen}
      />
      <TopTabs.Screen
        name="Tab3"
        options={{title: 'Drawer'}}
        component={RootDrawer}
      />
    </TopTabs.Navigator>
  );
}

export function RootDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="Drawer"
        component={Tab3Screen}
        options={{title: 'Drawer'}}
      />

      <Drawer.Screen
        name="Settings"
        component={SettingsScreen}
        options={{title: 'Settings'}}
      />
    </Drawer.Navigator>
  );
}
