export const MockInitialState = {
    "lastLogin": "Yaz has previously logged in",
    "jobs": {
        "sourceProvider": "Mock data",
        "items": [
            {
                "id": "73",
                "customerName": "Yaz",
                "status": 1,
                "dueTime": 1580197079722,
            },
            {
                "id": "72",
                "customerName": "Tom",
                "status": 1,
                "dueTime": 1580203428707,
            },
            {
                "id": "91",
                "customerName": "Paul",
                "status": 1
            }
        ]
    }
}