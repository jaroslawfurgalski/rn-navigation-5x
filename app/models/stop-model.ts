import { types } from 'mobx-state-tree'
import { AddressModel } from './address-model'
import { AirportModel } from './airport-model'
import { PassengerModel } from './passenger-model'

const StopModel = types.model('Stop', {
    class: types.string,
    id: types.string,
    index: types.maybe(types.integer),
    name: types.string,
    address: AddressModel,
    passengers: types.array(PassengerModel),
    freeWaitingTime: types.integer,
    instructions: types.maybe(types.string),
    airport: types.array(AirportModel),
})

export { StopModel }