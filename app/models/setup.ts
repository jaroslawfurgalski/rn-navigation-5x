import { RootModel } from "./root-model"
import { Root } from "./index"
import { Application } from "."
import { onSnapshot } from "mobx-state-tree"
import { EnvironmentConfig } from "../services"
import { Environment } from "./environment"

export async function createEnvironment(environmentConfig: EnvironmentConfig) {
  const env = new Environment(environmentConfig)
  await env.setup()
  return env
}

export async function setupRootStore (environmentConfig: EnvironmentConfig, seedData?: any) {
  const env = await createEnvironment(environmentConfig)

  const initialData: Application = seedData ?? {
    lastLogin: 'No previous login',
  }

  let rootTree: Root = RootModel.create({ application: initialData }, env)
  /* PLOP_INJECT_MST_READ_STORAGE */

  onSnapshot(rootTree, (snapshot) => {
    console.log('snapshot: ', snapshot)
    /* PLOP_INJECT_MST_WRITE_STORAGE */
  })
  return rootTree
}
/*

Example of how to support the root tree provider within the App component

export const App: React.FC<{}> = () => {
  const [rootStore, setRootStore] = useState<Root | undefined>(undefined) // prettier-ignore
  useEffect(() => {
    setupRootStore(null)
      .then(setRootStore) 
      .catch(e => {
        console.error('assign state fail: ', e)
      })
  }, [])

  if (!rootStore) {
    return null
  }  

  return (
    <Provider rootTree={rootStore}>
      <ThemeContext.Provider value={getTheme(uiTheme)}>
        <Navigator.withHeader />
      </ThemeContext.Provider>
    </Provider>
  );
}

*/

