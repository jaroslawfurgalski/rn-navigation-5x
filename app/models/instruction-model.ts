import { types } from 'mobx-state-tree'

const InstructionModel = types.model('Instruction', {
    type: types.string,
    value: types.string,
})

export { InstructionModel }