import { types } from 'mobx-state-tree'

const PassengerModel = types.model('Passenger', {
    name: types.string,
    telephone: types.string,
    accountNumber: types.integer,
    accountName: types.string,
    loyalty: types.maybe(types.string),
})

export { PassengerModel }