import { types } from 'mobx-state-tree'

const AirportModel = types.model('Airport', {
    name: types.string,
    terminal: types.string,
    flight: types.string,
    arrivalFrom: types.string,
})

export { AirportModel }