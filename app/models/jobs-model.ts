import { types, applySnapshot } from 'mobx-state-tree'
import { JobModel } from "./job-model"
import { Job } from "./"

const JobsModel = types.model('Jobs', {
    sourceProvider: types.string,
    activeJobId: types.maybe(types.string),
    items: types.array(JobModel)
}).actions(self => {
    function addJob(job: Job) {
        // Here is where we would add the job but not yet
        console.log('job added to state tree')
    }
    function setActiveJob(job: Job) {
        setActiveJobById(job.id)
    }
    function setActiveJobById(id: string) {
        applySnapshot(self, {
            ...self, 
            activeJobId: id 
        })
    }
    return { addJob, setActiveJob, setActiveJobById }
}).views(self => ({
    // Example of how to implement a view, in this case retrieve the active job
    get activeJob() {
        if (!self.activeJobId || !self.items) {
            return undefined
        }
        return self.items.find(j => j.id == self.activeJobId)
    }
}))

export { JobsModel }