import { types } from 'mobx-state-tree'

const GPSModel = types.model('GPS', {
    latitude: types.number,
    longitude: types.number,
})

export { GPSModel }