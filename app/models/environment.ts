import { 
    AuthenticationService,
    EnvironmentConfig,
} from "../services"
import { CommsManager } from "../services/brokers/comms-manager"

let ReactotronDev
/*
if (__DEV__) {
  const { Reactotron } = require("../services/reactotron")
  ReactotronDev = Reactotron
}
*/


/**
 * The environment is a place where services and shared dependencies between
 * models live.  They are made available to every model via dependency injection.
 */
export class Environment {
  constructor(environmentConfig: EnvironmentConfig) {
    // create each service
    /*
    if (__DEV__) {
      // dev-only services
      this.reactotron = new ReactotronDev()
    }
    */
    // Just flattening out from the config, nothing special
    this.options = environmentConfig.options
    this.authenticationService = environmentConfig.authenticationService
    this.commsManager = environmentConfig.commsManager
  }

  async setup() {
    // allow each service to setup
    /*
    if (__DEV__) {
      await this.reactotron.setup()
    }
    */
    // If there's any per-service initialisers to be called, do it here (doesn't apply to the auth service in this, but you get the idea)
    //await this.authenticationService.setup()
  }

  reactotron: typeof ReactotronDev

  options: any

  // Add properties for any service interfaces we need to expose.
  // The environment constructor will expect concrete implementations for these
  authenticationService: AuthenticationService

  commsManager: CommsManager
}
