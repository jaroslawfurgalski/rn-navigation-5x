import { types, applySnapshot, getEnv } from 'mobx-state-tree'
import { UserModel } from './user-model'
import { JobsModel } from './jobs-model'
import { Job } from '.'

const ApplicationModel = types.model('Application', {
    lastLogin: types.string,
    lastUser: types.maybe(UserModel),
    user: types.maybe(UserModel),
    jobs: types.maybe(JobsModel),
}).actions(self => {
    function newjob(job: Job) {
        const jobs = self.jobs || { items: [], sourceProvider: "default" }
        jobs.items.push(job)
        applySnapshot(self, {
            ...self, 
            jobs: jobs 
        })
    }
    function error(msg: String) {
        // loggingProvider = getEnv(self).loggingProvider...
    }
    // Action methods can be added like so. Actions may be added to any model within the
    // tree, so make sure you're doing things at a logically appropriate location within the tree
    function loggedIn(id: string, name: string) {
        const d = new Date()
        // This will apply the revised model tree, essentially giving us immutable state
        // because we should only be applying changes to state via the action methods
        applySnapshot(self, {
            ...self, 
            user: { id: id, name: name }, lastLogin: d.toLocaleTimeString() 
        })
        // In the above snapshot, we're spreading in *self* to seed the new state tree from
        // the existing, then we're explicitly setting the user model, obs.
    }
    // Any action methods you define must be referenced in the return object to be accessible
    return { newjob, error, loggedIn }
})

export { ApplicationModel }