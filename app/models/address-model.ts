import { types } from 'mobx-state-tree'
import { GPSModel } from './gps-model'

const AddressModel = types.model('Address', {
    company: types.maybe(types.string),
    building: types.maybe(types.string),
    street: types.maybe(types.string),
    postcode: types.maybe(types.string),
    city: types.maybe(types.string),
    w3w: types.maybe(types.string),
    gps: types.maybe(GPSModel),
})

export { AddressModel }