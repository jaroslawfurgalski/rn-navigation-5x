import { types, applySnapshot } from 'mobx-state-tree'

/*

-> from API (no jobStatus)

UI > accept job >
- call job.setStatus(JobStatus.Accepted)
 ? we've got no activeStopId yet
   if (!activeStopId) {
       activeStopId = self.stops[0].id
   }
  
-  if (self.jobStatus == JobStatus.AtStop) {
     // need a stop index 1..n
     if (we still have any stops in the list...) {
         activeStopId = activeStopId + 1...
     }
    }






*/

enum JobStatus {
    Received = 1,
    Acknowledged,
    Accepted,
    OnRouteToJob,
    AtStop,
    passengerOnBoard,
    OnWayToStop,
    // ... other statuses
    Complete,
}

const JobModel = types.model('Job', {
    id: types.string,
    
    customerName: types.string,
    dueTime: types.maybe(types.integer),
    /*
    Add required properties of a job, for instance stops, locations, whatever
    */
    status: types.number,
    //status: types.enumeration("JobStatus", [JobStatus.Unknown ]),
}).actions(self => {
    function setStatus(newStatus: JobStatus) {
            applySnapshot(self, {
            ...self, 
            status: newStatus
        })
    }
    return { setStatus }
})

export { JobModel, JobStatus }