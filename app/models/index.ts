import { Instance } from 'mobx-state-tree'
import { RootModel } from './root-model'
import { ApplicationModel } from './application-model'
import { JobsModel } from './jobs-model'
import { JobModel } from './job-model'
import { UserModel } from './user-model'

export { RootModel }
export { setupRootStore } from './setup'

export interface Root extends Instance<typeof RootModel> {}
export type Application = Instance<typeof ApplicationModel>
export type Jobs = Instance<typeof JobsModel>
export type Job = Instance<typeof JobModel>
export type User = Instance<typeof UserModel>